# Career

## Menambahkan Career

1. Pergi ke Dashboard admin website [IT Pro Indonesia](https://itpro.co.id/wp-login.php)

   <img src="../assets/images/step1-event.png">

2. Arahkan cursor ke menu **Career** lalu klik **Add New**

   <img src="../assets/images/step2-add-career.png">

3. Selanjutnya lengkapi form dengan data karir yang sudah tersedia

   - **Judul**
   - **Deskripsi**
   - **Tanggal**
   - **Experience Year**
   - **Responsibilities**
   - **Minimum Qualification**

    <br>
    <img src="../assets/images/step3-add-career.png">

    <img src="../assets/images/step4-add-career.png">
    <br>

4. Tambahkan **Form Karir** pada deskripsi block, klik tombol **Add Block** muncul popup klik **Contact Form**.

   <img src="../assets/images/step5-add-career.png">
   <img src="../assets/images/step6-add-career.png">

   Jika tidak ada pilihan **Contact Form** pada popup klik **Browse All** yang akan menampilkan komponen-komponen wordpress dan carilah komponen **Contact Form** disana

   <img src="../assets/images/step7-add-career.png">

5. Ganti pilihan form menjadi **Form Karir**

   <img src="../assets/images/step8-add-career.png">

6. Selesai, sekarang sudah bisa di publish. Tekan tombol **Publish**
