# Event

## Menambahkan Event

1. Pergi ke Dashboard admin website [IT Pro Indonesia](https://itpro.co.id/wp-login.php)

   <img src="../assets/images/step1-event.png">

2. Masuk ke menu event lalu klik "Add New"

   <img src="../assets/images/step2-event.png">

3. Masukkan data lengkap event ke dalam form yang tersedia

   <img src="../assets/images/step3-event.png">

4. Klik "Set Featured Image" pada kolom kanan form

   <img src="../assets/images/step4-event.png">

5. Akan muncul pilihan gambar. Jika gambar sudah diunggah terlebih dahulu maka bisa langsung memilih gambar yang sudah diunggah dan jika belum diunggah klik "Upload files" di pojok kiri atas. Jatuhkan gambarmu ke dalam kotak itu atau bisa mengunggahnya secara manual.

   <img src="../assets/images/step5-event.png">

   <img src="../assets/images/step6-event.png">

6. Jika sudah terisi semua bisa langsung klik tombol "Publish" di pojok kanan atas.

## Menghapus Event

1. Masuk ke menu event

   <img src="../assets/images/step2-event.png">

2. Pilih event yang ingin di hapus

   <img src="../assets/images/step1-event-delete.png">

3. Klik tombol "trash" dibawah judul event

   <img src="../assets/images/step2-event-delete.png">

::: tip INFO
Ini tidak akan menghapus event secara permanen, event hanya akan dipindahkan ke trash/sampah yang dimana nantinya bisa dikembalikan jika dibutuhkan kembali.
:::

## Restore Event

1. Masuk ke menu event

   <img src="../assets/images/step2-event.png">

2. Klik link "Trash"

   <img src="../assets/images/step1-restore-event.png">

3. Pilih event yang ingin dikembalikan lalu klik tombol "Restore"

   <img src="../assets/images/step2-restore-event.png">

## Menghapus Event Permanen

1. Masuk ke menu event

   <img src="../assets/images/step2-event.png">

2. Klik link "Trash"

   <img src="../assets/images/step1-restore-event.png">

3. Pilih event yang ingin dihapus permanen

   <img src="../assets/images/step1-remove-event.png">

## Pendaftaran Event

Berikut adalah cara untuk melihat siapa saja yang mendaftar event

1. Pergi ke Dashboard admin website [IT Pro Indonesia](https://itpro.co.id/wp-login.php)

   <img src="../assets/images/step1-event.png">

2. Klik link menu "Contact Form" yang terletak di sidebar menu

   <img src="../assets/images/step1-pendaftaran-event.png">

3. Klik link "Form Event"

   <img src="../assets/images/step2-pendaftaran-event.png">

   Database Form Event
   <img src="../assets/images/step3-pendaftaran-event.png">

   ::: tip INFO
   Untuk mengetahui event apa yang telah didaftarkan user, bisa dengan cara mengklik salah satu nama yang ada pada database, selanjutnya muncul data lengkap dari pendaftaran lalu salin post url event dan buka secara manual dengan browser.
   :::
   <img src="../assets/images/step4-pendaftaran-event.png">
